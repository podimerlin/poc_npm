# @podimerlin/poc_npm

## 0.0.1

### Patch Changes

- 327e0f4: update gitignore
- 327e0f4: asd

## 0.0.1-prerelease.4

### Patch Changes

- update gitignore

## 0.0.3-prerelease.0

### Patch Changes

- asd

## 0.0.2

### Patch Changes

- patch an update

## 0.0.1

### Patch Changes

- db8d718: init prerelease
- 2nd test for prerelease ssa
- try publish in gitlab

## 0.0.1-prerelease.3

### Patch Changes

- try publish in gitlab

## 0.0.1-prerelease.2

### Patch Changes

- 2nd test for prerelease

## 0.0.1-prerelease.1

### Patch Changes

- init prerelease
